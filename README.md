# Contracts in Javascript
All vanilla js - zero dependencies.

A contract is a function that validates a value's type.

If the value's type validates, the value is returned, otherwise a TypeError is thrown.

Check out [Mike Stay's videos](https://www.youtube.com/playlist?list=PLwuUlC2HlHGe7vmItFmrdBLn6p0AS8ALX) on category theory in Javascript.

## Functions:

### contracts
The main function returned from the module.

```javascript
// DEBUG === false bypasses type checking for production
const DEBUG = true
const contracts = require('contracts')
const {
	getType,
	assertType,
    C,
	assertAny,
	assertTruthy,
	assertFalsy,
	assertEmpty,
	assertNotEmpty,
	Type,
	guard
} = contracts(DEBUG)
```

### getType
`getType : Any -> String`

Return the string name of value's type.

```javascript
const cust = new Custom()
const obj = { someKey:'someValue' }
const arr = [1,2,3]
const re = /^hi/

// type strings returned from getType are case-sensitive
getType(cust) // "Custom"
getType(obj) // "Object"
getType(arr) // "Array"
getType(re) // "RegExp"
```

### assertType
`assertType : String -> Any -> Any | throw TypeError`

Create a contract for a value's type.
- If value passes the contract, the value is returned.
- If value does not pass the contract, a TypeError is thrown

```javascript
const assertString = assertType('String')

// type strings are case-sensitive
const assertNumber = assertType('number') // throws Error

const greet = (to) => {
	console.log(`Hello ${assertString(to)}`)
}

greet('World!') // 'Hello World!'

greet({text:'World!'}) // throws TypeError
```

##### Use assertType to create custom constracts.

```javascript
const assertArray = assertType('Array')
const assertNumber = assertType('Number')

const assertArrayOfNumbers = (xs) =>
  assertArray(xs).map(assertNumber)
```

### C.TYPE
`C.TYPE : Any -> Any | throw TypeError`

Helper functions for assertType.
  - It is recommended to use these over assertType.

```javascript
C.Number(5) // 5
C.Number('five') // throws TypeError
C.Object({ five: 5 }) // { five: 5 }
C.Object([ 1, 2, 3, 4, 5 ]) // throws TypeError
```

##### Create custom type helpers

```javascript
C.Future = assertType('Future')
C.IO = assertType('IO')

class Future { ... }
class IO { ... }

const f = new Future( ... )
const io = new IO( ... )

C.Future(f) // f
C.Future(io) // throws TypeError
C.IO(io) // io
C.IO(f) // throws TypeError

```

### assertTruthy
`assertTruthy : Any -> Any | throws TypeError`

Create a contract for [truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) values.

```javascript
assertTruthy(100) // 100
assertTruthy(0) // throws TypeError
assertTruthy('') // throws TypeError
```

### assertFalsy
`assertFalsy : Any -> Any | throws TypeError`

Create a contract for [falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy) values.

```javascript
assertTruthy(100) // throws TypeError
assertTruthy(0) // 0
assertTruthy('') // ''
```

### assertEmpty
`assertEmpty : Any -> Any | throws TypeError`

Create a contract for empty values.
- `{},[],0,'',undefined,null` are considered empty.

```javascript
assertEmpty(100) // throws TypeError
assertEmpty(0) // 0
assertEmpty('Hello') // throws TypeError
assertEmpty('') // ''
assertEmpty({}) // {}
```

### assertNotEmpty
`assertNotEmpty : Any -> Any | throws TypeError`

Create a contract for non-empty values.

```javascript
assertEmpty(100) // 100
assertEmpty(0) // throws TypeError
assertEmpty('Hello') // 'Hello'
assertEmpty('') // throws TypeError
assertEmpty({}) // throws TypeError
```

### Type
`Type : Object -> Function`

`Type : Array -> Function`

Create a contract for object or array values.

The returned function from Type is a contract.

```javascript
const Person = Type({
	age: C.String,
	name: {
		first: C.String,
		last: C.String
	}
})

// joe === {  age:30,  name: {  first:'Joe', last:'Shmoh'  } }
const joe = Person({
	age:30,
	name: {
		first:'Joe',
		last:'Shmoh'
	}
})

// throws TypeError for age not fulfilling contract of assertType(Number)
const jan = Person({
	age: '34',
	name: {
		first:'Jan',
		last:'McFailure'
	}
})

```

##### Use Type to create tuple-like structures.

```javascript
const XYZ = Type([
	C.Number,
	C.Number,
	C.Number
])

const coor = XYZ([20, 20, 0]) // [20, 20, 0]
const nextCoor = XYZ([100, 100]) // throws TypeError (missing value for contract at index 2)
const prevCoor = XYZ([0, undefined, 0]) // throws TypeError (index 1 fails 'number' contract)
```

##### Types can also be nested.

```javascript
const Sphere = Type({
	center: XYZ,
	radius: C.Number
})

// { center:[0, 100, 20], radius:20 }
const sphere = Sphere({
  center: [0, 100, 20],
  radius: 20
})

// invalid sphere throws TypeError
const nextSphere = Sphere({
  center: [0, 0, 0]
})
```

### guard
`guard : (Function a, b, c) -> Function`

Returns a guarded function.
  - Wraps a function in a contract for its input and output.

```javascript
// n-ary
const SumInput = Type([C.Number, C.Number])
const SumOutput = C.Number
const sum = guard( 
  (x, y) => x + y,
  SumInput,
  SumOutput
)

// Unary
const increment = guard(
    (n) => n + 1,
    C.Number,
    C.Number
)

sum(1, 2) // 3
sum('1', 2) // throws TypeError

increment(1) // 2
increment('1') // throws TypeError
```

_contracts.js_ is MIT licensed. see [LICENSE](https://gitlab.com/heycato/contracts/blob/master/LICENSE)
