module.exports = (DEBUG) => {

  const TYPE_NAMES = 
    [ 'Array',        'Boolean',     'Function',   'Null'
    , 'Number',       'Object',      'RegExp',     'String'
    , 'Undefined',    'Symbol',      'Map',        'WeakMap'
    , 'Set',          'WeakSet',     'Int8Array',  'Uint8Array'
    , 'Int16Array',   'Uint16Array', 'Int32Array', 'Uint32Array'
    , 'Float32Array', 'Float64Array'
    ]

  const getType = x => {
    const t1 = Object.prototype.toString.apply(x)
      .replace('[object ', '')
      .replace(']', '')
    return x && t1 != x.constructor.name ? x.constructor.name : t1
  }

  const assertType = type => x => {
    if(DEBUG) {
      if(type.toLowerCase() === type) {
        const corrected = type[0].toUpperCase() + type.slice(1)
        throw new Error(`Types are case-sensitive, did you mean ${corrected}?`)
      } else if(type !== getType(x)) {
        throw new TypeError(`Type of ${x} expected to be ${type}`)
      }
    }
    return x
  }

  const C =
    TYPE_NAMES
      .reduce((acc, typeName) => {
        acc[typeName] = assertType(typeName)
        return acc
      }, {})

  const typeOrNull = type => x =>
    getType(x) == type ? x : null

  const assertAny = x => x

  const assertTruthy = x => {
    if(!DEBUG) return x
    if(!!x) return x
    else throw new TypeError(`${x} expected to be truthy`)
  }

  const assertFalsy = x => {
    if(!DEBUG) return x
    if(!x) return x
    throw new TypeError(`${x} expected to be falsy`)
  }

  const assertEmpty = x => {
    let result
    switch(getType(x)) {
      case 'Object':
        result = Object.keys(x).length === 0 
        break
      case 'Number':
        result = x === 0
        break
      case 'String':
        result = x === ''
        break
      case 'RegExp':
        result = x.toString() === /(?:)/.toString()
        break
      case 'Undefined':
      case 'Null':
        result = true
        break
      case 'Boolean':
        result = !x
        break
      case 'Symbol':
        result = x.toString() === 'Symbol()'
        break
      case 'Map':
      case 'Set':
        result = x.size === 0
        break
      case 'WeakMap':
      case 'WeakSet':
        // weak references are assumed to be non-empty
        result = false
        break
      default:
        result = x.length === 0
        break
    }
    if(DEBUG && !result) {
      throw new Error(`Expected ${x} to be empty.`)
    } else {
      return x
    }
  }

  const assertNotEmpty = x => {
    let result
    try {
      result = assertEmpty(x)
    } catch(e) {
      return x
    }
    if(DEBUG) {
      throw new Error(`Expected ${x} to be not empty.`)
    } else {
      return x
    }
  }

  const guard = (fn, assertInput, assertOutput) =>
    (...args) =>
      assertOutput(
        fn.apply(
          null,
          [].concat(
            args.length > 1 ?
              assertInput(args) :
              assertInput(args[0])
          )
        )
      )

  const Type = (assertions) =>
    DEBUG ? 
      (value) => 
        Array.isArray(assertions) ?
          assertions.map((assert, i) => {
            try {
              return assert(value[i])
            } catch(e) {
              if(getType(value[i]) == 'Undefined') {
                throw new TypeError(`Index ${i} missing from type [${Object.keys(assertions)}]`)
              } else {
                throw e
              }
            }
          }) :
          Object.keys(C.Object(assertions))
            .reduce((acc, key) => {
              if(/Array|Object/.test(getType(assertions[key]))) {
                acc[key] = Type(assertions[key])(value[key])
              } else {
                try {
                  acc[key] = C.Function(assertions[key])(value[key])
                } catch(e) {
                  if(getType(value[key]) == 'Undefined') {
                    throw new TypeError(`${key} missing from type {${Object.keys(assertions)}}`)
                  } else {
                    throw e
                  }
                }
              }
              return acc
            }, {}) :
      (value) => value
    
  return {
    assertType,
    assertAny,
    assertTruthy,
    assertFalsy,
    assertEmpty,
    assertNotEmpty,
    getType,
    guard,
    typeOrNull,
    Type,
    C,
    TYPE_NAMES
  }
}
