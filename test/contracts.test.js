class Custom { constructor() {} }

const obj = { one: 1 }
const arr = [ 1, 2, 3 ]
const num = 1
const str = 'Hello World'
const fn = () => {}
const re = new RegExp('hi', 'g')
const undef = undefined
const nul = null
const cust = new Custom()

const keyedEmptyValues =
  [ [ 'Array',             [] ]
  , [ 'Boolean',           false ]
  , [ 'Function',          () => {} ]
  , [ 'Null',              null ]
  , [ 'Number',            0 ]
  , [ 'Object',            {} ]
  , [ 'RegExp',            new RegExp() ]
  , [ 'String',            '' ]
  , [ 'Undefined',         undefined ]
  , [ 'Symbol',            Symbol() ]
  , [ 'Map',               new Map() ]
  , [ 'Set',               new Set() ]
  , [ 'Int8Array',         new Int8Array() ]
  , [ 'Uint8Array',        new Uint8Array() ]
  , [ 'Int16Array',        new Int16Array() ]
  , [ 'Uint16Array',       new Uint16Array() ]
  , [ 'Int32Array',        new Int32Array() ]
  , [ 'Uint32Array',       new Uint32Array() ]
  , [ 'Float32Array',      new Float32Array() ]
  , [ 'Float64Array',      new Float64Array() ]
  ]

const keyedNotEmptyValues =
  [ [ 'Array',             [1,2,3] ]
  , [ 'Boolean',           true ]
  , [ 'Function',          (a, b) => a + b ]
  , [ 'Number',            10 ]
  , [ 'Object',            {one:1} ]
  , [ 'RegExp',            new RegExp('not empty', 'g') ]
  , [ 'String',            'not empty' ]
  , [ 'Symbol',            Symbol('five') ]
  , [ 'Map',               new Map([[1,2], [3,4]]) ]
  , [ 'WeakMap',           new WeakMap([[{one:1},'world']]) ]
  , [ 'Set',               new Set([1,2,3,4]) ]
  , [ 'Int8Array',         new Int8Array(2) ]
  , [ 'Uint8Array',        new Uint8Array(2) ]
  , [ 'Int16Array',        new Int16Array(2) ]
  , [ 'Uint16Array',       new Uint16Array(2) ]
  , [ 'Int32Array',        new Int32Array(2) ]
  , [ 'Uint32Array',       new Uint32Array(2) ]
  , [ 'Float32Array',      new Float32Array(2) ]
  , [ 'Float64Array',      new Float64Array(2) ]
  ]

const truthyKeyValues =
  keyedEmptyValues
    .filter(([typeName, value]) => !!value)

const falsyKeyValues =
  keyedEmptyValues
    .filter(([typeName, value]) => !value)

describe('DEVELOPMENT MODE (DEBUG === true)', () => {
  const DEBUG = true

  const { assertType
        , assertAny
        , assertTruthy
        , assertFalsy
        , assertEmpty
        , assertNotEmpty
        , getType
        , guard
        , typeOrNull
        , Type
        , C
        , TYPE_NAMES
        } = require('../index.js')(DEBUG)

  describe('Not DEBUG dependent - these do not throw', () => {
    test('getType', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) =>
          expect(getType(value)).toBe(typeName)
        )
    })

    test('typeOrNull', () => {
      const getOffsetValue = (arr, i) => 
        arr[i + 1] !== undefined ?
          arr[i + 1][1] :
          arr[0][1]
      keyedEmptyValues
        .forEach(([typeName, value], i, arr) => {
          expect(typeOrNull(typeName)(value)).toBe(value)
          expect(typeOrNull(typeName)(getOffsetValue(arr, i))).toBe(null)
        })
    })

    test('assertAny', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) =>
          expect(assertAny(value)).toBe(value)
        )
    })
  })

  describe('assertTruthy/Falsy', () => {
    test('assertTruthy pass through truthy values', () => {
      truthyKeyValues
        .forEach(([typeName, value]) => 
          expect(assertTruthy(value)).toBe(value)
        )
    })

    test('assertTruthy throws on non-truthy values', () => {
      falsyKeyValues
        .forEach(([typeName, value]) => 
          expect(() => assertTruthy(value)).toThrow()
        )
    })

    test('assertFalsy pass through falsy values', () => {
      falsyKeyValues
        .forEach(([typeName, value]) => 
          expect(assertFalsy(value)).toBe(value)
        )
    })

    test('assertFalsy throws on non-falsy values', () => {
      truthyKeyValues
        .forEach(([typeName, value]) => 
          expect(() => assertFalsy(value)).toThrow()
        )
    })
  })

  describe('assertEmpty/NotEmpty', () => {
    test('assertEmpty(TYPE) pass through valid empty values', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(assertEmpty(value)).toBe(value)
        )
    })

    test('assertEmpty(TYPE) throw on non-empty values', () => {
      keyedNotEmptyValues
        .forEach(([typeName, value]) => 
          expect(() => assertEmpty(value)).toThrow()
        )
    })

    test('assertNotEmpty(TYPE) pass through valid non-empty values', () => {
      keyedNotEmptyValues
        .forEach(([typeName, value]) => 
          expect(assertNotEmpty(value)).toBe(value)
        )
    })

    test('assertNotEmpty(TYPE) throw on empty values', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(() => assertNotEmpty(value)).toThrow()
        )
    })
  })

  describe('assertType', () => {

    test('assertType(TYPE) returns an assertion function', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(getType(assertType(typeName))).toBe('Function')
        )
    })

    test('assertType(TYPE), TYPE is case-sensitive', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(() => assertType(typeName.toLowerCase())(value)).toThrow()
        )
    })

    test('C.TYPE helpers pass through on valid value', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(C[typeName](value)).toBe(value)
        )
    })

    test('C.TYPE helpers throw on invalid value', () => {
      keyedEmptyValues
        .forEach(([typeName, value], i, arr) => {
          let invalid = arr[i + 1] ? arr[i + 1][1] : arr[0][1]
          expect(() => C[typeName](arr[i + 1][1])).toThrow()
        })
    })

    test('assertType for custom types', () => {
      expect(assertType('Custom')(cust)).toBe(cust)
    })

  })

  describe('guard', () => {
    const SumInput = Type([C.Number, C.Number])
    const sum = guard(
      (x, y) => x + y,
      SumInput,
      C.Number
    )

    const increment = guard(
      (n) => n + 1,
      C.Number,
      C.Number
    )

    const wrongOutput = guard(
      (n) => n + 1,
      C.Number,
      C.Array,
    )

    test('Creates a guarded function', () => {
      expect(getType(sum)).toBe('Function')
    })

    test('Function for Unary', () => {
      expect(increment(1)).toBe(2)
    })

    test('Function for n-ary', () => {
      expect(sum(1, 2)).toBe(3)
    })

    test('Function throws on failed contracted input values', () => {
      expect(() => sum('1', 2)).toThrow()
    })

    test('Function throws on failed contracted output value', () => {
      expect(() => wrongOutput(2)).toThrow()
    })
  })

  describe('Type', () => {
    const Person = Type({
      age: C.Number,
      name: {
        first: C.String,
        last: C.String
      }
    })

    const XYZ = Type([
      C.Number,
      C.Number,
      C.Number
    ])

    test('Type should return a function', () => {
      expect(getType(Person)).toBe('Function')
    })

    test('Type Person should return valid type', () => {
      const joe = {
        age:35,
        name: {
          first: 'Joe',
          last: 'Dirt'
        }
      }
      const p = Person(joe)
      expect(p).toEqual(joe)
    })

    test('Type Person should throw on invalid property types', () => {
      const joe = {
        age:'35',
        name: {
          first: 'Joe',
          last: 'Dirt'
        }
      }
      expect(() => Person(joe)).toThrow()
    })

    test('Type can be used for tuples', () => {
      expect(XYZ([1,2,3])).toEqual([1,2,3])
    })

    test('Tuple throws on invalid type value', () => {
      expect(() => XYZ([1,'invalid',3])).toThrow()
    })

    test('Tuple throws on undefined required value', () => {
      expect(() => XYZ([1,3])).toThrow()
    })

    test('Types can be nested', () => {
      const Circle = Type({
        radius: C.Number,
        center: XYZ
      })

      const circle = Circle({
        radius: 5,
        center: [20,20,0]
      })

      expect(circle).toEqual({
        radius: 5,
        center: [20,20,0]
      })
    })

    test('Types throw when key is undefined on value', () => {
      const Circle = Type({
        radius: C.Number,
        center: XYZ
      })

      expect(() => Circle({ center: [20,20,0] })).toThrow()
    })

  })
})

describe('PRODUCTION MODE (DEBUG === false)', () => {
  const DEBUG = false

  const { assertType
        , assertAny
        , assertTruthy
        , assertFalsy
        , assertEmpty
        , assertNotEmpty
        , getType
        , guard
        , Type
        , C
        } = require('../index.js')(DEBUG)

  describe('assertTruthy/Falsy', () => {
    test('assertTruthy pass through truthy values', () => {
      truthyKeyValues
        .forEach(([typeName, value]) =>
          expect(assertTruthy(value)).toBe(value)
        )
    })

    test('assertTruthy does not throw on non-truthy values', () => {
      falsyKeyValues
        .forEach(([typeName, value]) =>
          expect(assertTruthy(value)).toBe(value)
        )
    })

    test('assertFalsy pass through falsy values', () => {
      falsyKeyValues
        .forEach(([typeName, value]) =>
          expect(assertFalsy(value)).toBe(value)
        )
    })

    test('assertFalsy does not throw on non-falsy values', () => {
      truthyKeyValues
        .forEach(([typeName, value]) =>
          expect(assertFalsy(value)).toBe(value)
        )
    })
  })

  describe('assertEmpty/NotEmpty', () => {
    test('assertEmpty(TYPE) pass through valid empty values', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) =>
          expect(assertEmpty(value)).toBe(value)
        )
    })

    test('assertEmpty(TYPE) to not throw on non-empty values', () => {
      keyedNotEmptyValues
        .forEach(([typeName, value]) =>
          expect(assertEmpty(value)).toBe(value)
        )
    })

    test('assertNotEmpty(TYPE) pass through valid non-empty values', () => {
      keyedNotEmptyValues
        .forEach(([typeName, value]) =>
          expect(assertNotEmpty(value)).toBe(value)
        )
    })

    test('assertNotEmpty(TYPE) to not throw on empty values', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) =>
          expect(assertNotEmpty(value)).toBe(value)
        )
    })
  })

  describe('assertType', () => {
    test('assertType(TYPE) returns an assertion function', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(getType(assertType(typeName))).toBe('Function')
        )
    })

    test('assertType(TYPE), TYPE is case-sensitive', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(assertType(typeName.toLowerCase())(value)).toBe(value)
        )
    })

    test('C.TYPE helpers pass through valid value', () => {
      keyedEmptyValues
        .forEach(([typeName, value]) => 
          expect(C[typeName](value)).toBe(value)
        )
    })

    test('C.TYPE helpers pass through on invalid value', () => {
      keyedEmptyValues
        .forEach(([typeName, value], i, arr) => {
          let invalid = arr[i + 1] ? arr[i + 1][1] : arr[0][1]
          expect(C[typeName](invalid)).toBe(invalid)
        })
    })

  })

  describe('guard', () => {
    const SumInput = Type([C.Number, C.Number])
    const sum = guard(
      (x, y) => x + y,
      SumInput,
      C.Number
    )

    const wrongOutput = guard(
      (n) => n + 1,
      C.Number,
      C.Array
    )

    test('Function does not throw on failed contracted input values', () => {
      expect(sum('1', 2)).toBe('12')
    })

    test('Function does not throw on failed contracted output value', () => {
      expect(wrongOutput(2)).toBe(3)
    })
  })

  describe('Type', () => {
    const Person = Type({
      age: C.Number,
      name: {
        first: C.String,
        last: C.String
      }
    })

    const XYZ = Type([
      C.Number,
      C.Number,
      C.Number
    ])

    test('Type should return a function', () => {
      expect(getType(Person)).toBe('Function')
    })

    test('Type Person should return valid type', () => {
      const joe = {
        age:35,
        name: {
          first: 'Joe',
          last: 'Dirt'
        }
      }
      const p = Person(joe)
      expect(p).toEqual(joe)
    })

    test('Type Person should not throw on invalid property types', () => {
      const joe = {
        age:'35',
        name: {
          first: 'Joe',
          last: 'Dirt'
        }
      }
      expect(Person(joe)).toEqual(joe)
    })

    test('Type can be used for tuples', () => {
      expect(XYZ([1,2,3])).toEqual([1,2,3])
    })

    test('Tuple should not throw on invalid type value', () => {
      expect(XYZ([1,'invalid',3])).toEqual([1,'invalid',3])
    })

    test('Tuple should not throw on undefined required value', () => {
      expect(XYZ([1,3])).toEqual([1,3])
    })

    test('Types can be nested', () => {
      const Circle = Type({
        radius: C.Number,
        center: XYZ
      })

      const circle = Circle({
        radius: 5,
        center: [20,20,0]
      })

      expect(circle).toEqual({
        radius: 5,
        center: [20,20,0]
      })
    })

    test('Types to not throw when key is undefined on value', () => {
      const Circle = Type({
        radius: C.Number,
        center: XYZ
      })

      const circle = Circle({
        center: [20,20,0]
      })

      expect(circle).toEqual({
        center: [20,20,0]
      })
    })
  })
})
